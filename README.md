# Spring boot [Montreal Exame Tecnico]

# Execução e testes unitários:

Para executar o projeto:
```bash
mvn spring-boot:run
```

Para executar os testes:

```bash
mvn verify
``` 
Este projeto e um serviço Restful para executar as operações crud sobre um produto e seu sub recurso imagem.

Toda a documentação do projeto foi realizada utilizando o projeto [Spring Rest Docs](https://projects.spring.io/spring-restdocs/) .

Portanto um documento chamado (montrealDocs.html) esta disponível na raiz do projeto contendo toda a documentação e exemplos para o a utilização da API. por questões de segurança o bitbucket não disponibiliza um visualizador de arquivos html para arquivos fonte do projeto, Peco a gentileza que o arquivo seja baixado e visualizado em um browser.

Além disso após iniciar a aplicação ao acessar o endereço http://localhost:8080/ o [hal-browser](https://github.com/mikekelly/hal-browser) esta pronto para uso, possibilitando assim uma interface gráfica para visualização e utilização de toda a API.

Busquei utilizar ao máximo os padrões e facilidades oferecidas pelos projetos spring data-jpa,rest,doc e test. Utilizei também o [Projeto lombok](http://jnb.ociweb.com/jnb/jnbJan2010.html) para, onde possível, evitar códigos do tipo "Boilerplate".

Para o resource Produto busquei utilizar os métodos gerados através do repositório via spring-rest.
Métodos negociais além do escopo da CRUD utilizei o spring-data.
Para o sub recurso de imagens, criei um controller e utilizei uma interface de serviço para intermediar o acesso aos dados definindo uma API de acesso neste serviço.