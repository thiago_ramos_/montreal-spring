package br.com.projeto.montreal.data.repository;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import br.com.projeto.montreal.model.entities.Image;
import br.com.projeto.montreal.model.entities.projection.ImageSummaryProjection;

@RepositoryRestResource(exported=false,excerptProjection=ImageSummaryProjection.class)
public interface IImageRepository extends PagingAndSortingRepository<Image, Long> {
	
	/**
	 * Gets the by product id.
	 *
	 * @param productId
	 *            the product id
	 * @return the by product id
	 */
	List<Image> getByProductId(@Param("productId") Long productId);

}