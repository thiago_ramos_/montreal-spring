package br.com.projeto.montreal.data.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import br.com.projeto.montreal.model.entities.Product;

@RepositoryRestResource
public interface IProductRepository
		extends PagingAndSortingRepository<Product, Long> {

	/**
	 * Find all by parent product id.
	 *
	 * @param parentProductId
	 *            the parent product id
	 * @return the list
	 */
	@RestResource(rel="byParentId",path="byParentId")
	List<Product> findAllByParentProductId(
			@Param("parentProductId") Long parentProductId);

	/**
	 * Product fetch childrens by id.
	 *
	 * @param id
	 *            the id
	 * @return the product
	 */
	@RestResource(rel="byIdFetchSubProducts",path="productFetchSubProducts")
	@Query("select p from Product p left join p.subProducts where p.id = :id")
	Product productFetchChildrensById(@Param("id") Long id);

	/**
	 * Product fetch images by id.
	 *
	 * @param id
	 *            the id
	 * @return the product
	 */
	@RestResource(rel="byIdFetchImages",path="productFetchImages")
	@Query("select p from Product p left join p.images where p.id = :id")
	Product productFetchImagesById(@Param("id") Long id);
	
	/**
	 * All fetch childrens.
	 *
	 * @return the list
	 */
	@RestResource(rel="allFetchSubProducts",path="allFetchSubProducts")
	@Query("select p from Product p left join p.subProducts")
	List<Product> allFetchChildrens();
	
	/**
	 * With fetch images.
	 *
	 * @param pageable
	 *            the pageable
	 * @return the list
	 */
	@RestResource(rel="allFetchImages",path="allFetchImages")
	@Query("select p from Product p left join fetch p.images")
	List<Product> withFetchImages(Pageable pageable);
	
	


}
