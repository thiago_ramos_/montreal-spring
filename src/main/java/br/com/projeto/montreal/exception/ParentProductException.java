/*
 * Copyright 2012-2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package br.com.projeto.montreal.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import br.com.projeto.montreal.model.entities.Product;


/**
 *  Exception being thrown in case an {@link Product} has a set of sub products associated.
 *
 * @author thiago
 * @since 13/05/2017
 */
@ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
public class ParentProductException extends RuntimeException {

	private static final long serialVersionUID = -3713604938650936744L;


}
