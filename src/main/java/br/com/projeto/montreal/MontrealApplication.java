package br.com.projeto.montreal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Classe central da aplicacao. Mantem as configuracoes gerais da aplicacao.
 *
 * @author thiago
 */
@SpringBootApplication
public class MontrealApplication {

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(MontrealApplication.class, args);
	}
}
