package br.com.projeto.montreal;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import br.com.projeto.montreal.data.repository.IProductRepository;
import br.com.projeto.montreal.model.entities.Image;
import br.com.projeto.montreal.model.entities.Product;
import br.com.projeto.montreal.model.enums.MimeType;
import lombok.extern.slf4j.Slf4j;

/**
 * Inicializador para registros de {@link Product}
 * 
 * @author thiago
 * @since 11/05/2017
 */
@Slf4j
@Service
public class ProductInitializer {
	
	@Autowired
	private IProductRepository productRepository;

	@EventListener
	public void init(ApplicationReadyEvent event) {
		
		List<Image> picList = new ArrayList<Image>();
		List<Product> productList = new ArrayList<Product>();
		
		picList.add(new Image(MimeType.JPEG));
		picList.add(new Image(MimeType.BMP));
		picList.add(new Image(MimeType.TIFF));
		
		productList.add(new Product("Mochila","Um produto da prefeitura de jequie.",picList));
		productList.add(new Product("Pasta","Jaiminho Collector´s Edition.",productList.get(0)));
		
		productList.add(new Product("Calcados","Secao principal.",picList));
		productList.add(new Product("Chinelo","Flip Flop.",picList.subList(0, 1)));
		productList.add(new Product("Tenis","Tenis de corrida.",productList.get(2),picList));
		productList.add(new Product("Crocs","WTF?!?!",productList.get(2),picList));
		productList.add(new Product("Pai sem filho","WCIS"));
		
		List<Product> cargaInicialList = (List<Product>) productRepository.save(productList);
		
		LOG.info("========================{} Produtos para carga inicial foram criados!!",cargaInicialList.size());
	}
}
