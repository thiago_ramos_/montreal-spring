/**
 * 
 */
package br.com.projeto.montreal.model.entities.projection;

import org.springframework.data.rest.core.config.Projection;

import br.com.projeto.montreal.model.entities.Product;

/**
 * Projection interface for {@link Product}.
 * 
 * @author thiago
 *
 */
@Projection(name = "summary" , types =  Product.class)
public interface ProductSummaryProjection {
	
	String getName();
	
	String getDescription();
}
