/**
 * 
 */
package br.com.projeto.montreal.model.entities.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import br.com.projeto.montreal.model.entities.Product;

/**
 * Projection interface for {@link Product}.
 * 
 * @author thiago
 *
 */
@Projection(name = "withImages" , types =  Product.class)
public interface ProductAndImagesProjection {
	
	String getName();
	
	String getDescription();
	
	@Value("#{target.images.toString()}")
	String getImages();
}
