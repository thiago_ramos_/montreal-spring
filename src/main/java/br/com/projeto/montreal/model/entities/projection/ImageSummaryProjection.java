package br.com.projeto.montreal.model.entities.projection;

import org.springframework.data.rest.core.config.Projection;

import br.com.projeto.montreal.model.entities.Image;

@Projection(name = "imgSummary" , types =  Image.class)
public interface ImageSummaryProjection {
	
	String getType();

}
