/**
 * 
 */
package br.com.projeto.montreal.model.entities.projection;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import br.com.projeto.montreal.model.entities.Product;

/**
 * Projection interface for {@link Product}.
 * 
 * @author thiago
 *
 */
@Projection(name = "withFullGraph" , types =  Product.class)
public interface ProductFullGraphProjection {
	
	String getName();
	
	String getDescription();
	
	@Value("#{target.subProducts.toString()}")
	String getSubProducts();
}
