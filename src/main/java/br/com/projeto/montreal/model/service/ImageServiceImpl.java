package br.com.projeto.montreal.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.projeto.montreal.data.repository.IImageRepository;
import br.com.projeto.montreal.model.entities.Image;
import br.com.projeto.montreal.model.entities.Product;

/**
 * Implementation of {@link IImageService} delegating persistence and business
 * operations to {@link IImageRepository}
 *
 * @author thiago
 * @since 13/05/2017
 */
@Service
public class ImageServiceImpl implements IImageService {

	@Autowired
	private IImageRepository imageRepository;

	@Override
	public List<Image> getAllImagensFromProdut(Product product) {
		return imageRepository.getByProductId(product.getId());
	}

}
