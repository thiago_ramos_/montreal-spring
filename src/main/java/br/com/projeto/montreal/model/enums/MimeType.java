package br.com.projeto.montreal.model.enums;

/**
 * The Enum MimeType.
 *
 * @author thiago
 */
public enum MimeType {
	BMP,TIFF,JPEG;
}
