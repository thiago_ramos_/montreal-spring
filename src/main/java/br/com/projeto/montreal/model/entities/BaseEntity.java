package br.com.projeto.montreal.model.entities;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import org.springframework.hateoas.Identifiable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * Classe ancestral para todas as entidades do sistema.
 *
 * @author thiago
 */
@MappedSuperclass
@Getter
@ToString
@EqualsAndHashCode
public class BaseEntity implements Identifiable<Long>	 {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@JsonIgnore
	private final Long id;

	@Version
	private Long version;

	protected BaseEntity() {
		this.id = null;
	}

}
