package br.com.projeto.montreal.model.entities;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Builder;
import lombok.NonNull;
import lombok.ToString;

/**
 * Representa um produto
 * 
 * @author thiago
 */

@Table(name = "PRODUCT")
@Entity
@JsonInclude(JsonInclude.Include.NON_NULL)
@ToString(exclude={"subProducts","images"})
public class Product extends BaseEntity {

	@NotBlank
	@NonNull
	private String name;

	@NotBlank
	@NonNull
	private String description;

	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, optional = true)
	private Product parentProduct;

	@OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private Set<Image> images;

	@OneToMany(mappedBy = "parentProduct", cascade = CascadeType.ALL)
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	private Set<Product> subProducts;

	/**
	 * Instantiates a new {@link Product} with name, description parent product
	 * and images.
	 *
	 * @param productName
	 *            the product name
	 * @param productDescription
	 *            the product description
	 * @param parentProduct
	 *            the parent product
	 * @param images
	 *            the product images
	 */
	public Product(String productName, String productDescription,
			Product parentProduct, List<Image> images) {
		this.name = StringUtils.isEmpty(productName) ? "Produto Generico"
				: productName;
		this.description = StringUtils.isEmpty(productName) ? "NA"
				: productDescription;
		this.parentProduct = parentProduct;
		addImages(images);
	}

	/**
	 * Instantiates a new {@link Product} with name, description and parent
	 * product.
	 *
	 * @param productName
	 *            the product name
	 * @param productDescription
	 *            the product description
	 * @param parentProduct
	 *            the parent product
	 */
	public Product(String productName, String productDescription,
			Product parentProduct) {
		this.name = StringUtils.isEmpty(productName) ? "Produto Generico"
				: productName;
		this.description = StringUtils.isEmpty(productName) ? "NA"
				: productDescription;
		this.parentProduct = parentProduct;
	}

	/**
	 * Instantiates a new {@link Product} with name, description and images.
	 *
	 * @param productName
	 *            the product name
	 * @param productDescription
	 *            the product description
	 * @param images
	 *            the product images
	 */
	public Product(String productName, String productDescription,
			List<Image> images) {
		this.name = StringUtils.isEmpty(productName) ? "Produto Generico"
				: productName;
		this.description = StringUtils.isEmpty(productName) ? "NA"
				: productDescription;
		addImages(images);
	}

	/**
	 * Instantiates a new product.
	 *
	 * @param name
	 *            the name
	 * @param description
	 *            the description
	 */
	@Builder
	public Product(String name, String description) {
		this.name = name;
		this.description = description;
	}

	private void addImages(List<Image> images) {
		if (!CollectionUtils.isEmpty(images)) {
			images.forEach(pi -> pi.setProduct(this));
			if (this.images == null) {
				this.images = new HashSet<>();
			}
			this.images.addAll(images);
		}
	}

	@JsonIgnore
	public boolean isParent() {
		return !CollectionUtils.isEmpty(this.subProducts);
	}

	public Product() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Product getParentProduct() {
		return parentProduct;
	}

	public void setParentProduct(Product parentProduct) {
		this.parentProduct = parentProduct;
	}

	public Set<Image> getImages() {
		return images;
	}

	public void setImages(Set<Image> images) {
		this.images = images;
	}

	public Set<Product> getSubProducts() {
		return subProducts;
	}

	public void setSubProducts(Set<Product> subProducts) {
		this.subProducts = subProducts;
	}
}
