package br.com.projeto.montreal.model.service;

import java.util.List;

import br.com.projeto.montreal.model.entities.Image;
import br.com.projeto.montreal.model.entities.Product;

/**
 * Interface to domain services from {@link Product.}
 *
 * @author thiago
 * @since 13/05/2017
 */
public interface IImageService {

	/**
	 * Gets the all imagens from produt.
	 *
	 * @param product
	 *            the product
	 * @return the {@link Image} list for the given {@link Product} or
	 *         {@literal null} if the {@link Product} does not have images.
	 */
	public List<Image> getAllImagensFromProdut(Product product);

}
