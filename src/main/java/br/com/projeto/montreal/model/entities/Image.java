package br.com.projeto.montreal.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import br.com.projeto.montreal.model.enums.MimeType;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

/**
 * Representa a imagem associada ao um {@link Product}
 *
 * @author thiago
 */
@Table(name = "PRODUCT_IMG")
@Entity
@NoArgsConstructor(force = true)
@EqualsAndHashCode(callSuper = false)
@RequiredArgsConstructor
@ToString(exclude="product")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Image extends BaseEntity {

	@NonNull
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "TYPE", nullable = false)
	private MimeType type;

	@NonNull
	@ManyToOne
	@JoinColumn(name = "PRODUCT_ID")
	@JsonIgnore
	private Product product;

	/**
	 * Instantiates a new product image.
	 *
	 * @param type
	 *            the type
	 */
	public Image(MimeType type) {
		this.type = type;
	}

	public MimeType getType() {
		return type;
	}

	public void setType(MimeType type) {
		this.type = type;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}
}
