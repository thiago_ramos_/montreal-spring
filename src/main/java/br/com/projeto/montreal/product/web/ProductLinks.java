package br.com.projeto.montreal.product.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Component;

import br.com.projeto.montreal.model.entities.Product;
import lombok.RequiredArgsConstructor;

/**
 * The Class ProductLinks.
 *
 * @author thiago
 * @since 16/05/2017
 */
@Component
@RequiredArgsConstructor
public class ProductLinks {
	
	static final String IMAGES = "images";
	static final String IMAGES_REL = "images";
	
	@Autowired
	private EntityLinks entityLinks;

	/**
	 * Gets the images link.
	 *
	 * @param product
	 *            the product
	 * @return the images link
	 */
	public Link getImagesLink(Product product) {
		return entityLinks.linkForSingleResource(product).slash(IMAGES).withRel(IMAGES_REL);
	}

}
