package br.com.projeto.montreal.product.web;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

import br.com.projeto.montreal.model.entities.Product;
import lombok.extern.slf4j.Slf4j;

/**
 * The Class ProductResourceProcessor.
 *
 * @author thiago
 * @since 16/05/2017
 */
@Component
@Slf4j
public class ProductResourceProcessor implements ResourceProcessor<Resource<Product>> { 
	
	@Autowired
	private ProductLinks productLinks;

	/* (non-Javadoc)
	 * @see org.springframework.hateoas.ResourceProcessor#process(org.springframework.hateoas.ResourceSupport)
	 */
	@Override
	public Resource<Product> process(Resource<Product> resource) {
		Product product = resource.getContent();
		
		try {
			resource.add(productLinks.getImagesLink(product));	
		} catch (IllegalArgumentException e) {
			LOG.info("No link for images");
		}
		return resource;
	}

	

}
