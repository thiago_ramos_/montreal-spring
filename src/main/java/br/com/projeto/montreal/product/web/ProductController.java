package br.com.projeto.montreal.product.web;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.projeto.montreal.model.entities.Image;
import br.com.projeto.montreal.model.entities.Product;
import br.com.projeto.montreal.model.service.IImageService;
import lombok.extern.slf4j.Slf4j;

/**
 * Controller to handle images resources of {@link Product}
 *
 * @author thiago
 * @since 13/05/2017
 */
@Controller
@RequestMapping("/products/{id}")
@ExposesResourceFor(Product.class)
@Slf4j
public class ProductController {

	@Autowired
	private IImageService imageService;

	/**
	 * Get a {@link Image} list from an {@link Product}
	 * 
	 * @param product
	 *            the {@link Product} for images search. Will be {@literal null}
	 *            in case no {@link Product} with the given id could be found.
	 * @return
	 */
	@RequestMapping(path = ProductLinks.IMAGES, method = GET)
	HttpEntity<?> getImages(@PathVariable("id") Product product) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("<<getImages for product with id: " + product.getId());
		}
		List<Image> imageList = imageService.getAllImagensFromProdut(product);
		return CollectionUtils.isEmpty(imageList)
				? new ResponseEntity<>(HttpStatus.NOT_FOUND)
				: ResponseEntity.ok(imageList);
	}

}
