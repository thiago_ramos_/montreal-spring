package br.com.projeto.montreal;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class BaseMontrealTests.
 *
 * @author thiago
 * @since 13/05/2017
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@SpringBootTest
public abstract class BaseMontrealTests {

}
