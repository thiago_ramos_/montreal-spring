package br.com.projeto.montreal;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.LinkDiscoverer;
import org.springframework.hateoas.LinkDiscoverers;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;

@Component
public class LinkMatcher implements ResultMatcher {

	@Autowired
	LinkDiscoverers links;

	private String rel;
	private boolean isPresent;

	public LinkMatcher(String rel, boolean present) {
		this.rel = rel;
		this.isPresent = present;
	}
	
	public LinkMatcher() {
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.springframework.test.web.servlet.ResultMatcher#match(org.
	 * springframework.test.web.servlet.MvcResult)
	 */
	@Override
	public void match(MvcResult result) throws Exception {

		MockHttpServletResponse response = result.getResponse();
		String content = response.getContentAsString();
		LinkDiscoverer discoverer = links
				.getLinkDiscovererFor(response.getContentType());

		assertThat(discoverer.findLinkWithRel(rel, content),
				is(isPresent ? notNullValue() : nullValue()));
	}

}
