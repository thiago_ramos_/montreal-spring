package br.com.projeto.montreal;

import br.com.projeto.montreal.model.entities.Product;

/**
 * The Class TestUtils.
 *
 * @author thiago
 * @since 13/05/2017
 */
public class TestUtils {
	
	/**
	 * Builds the product.
	 *
	 * @return the product
	 */
	public static Product buildProduct(){
		return Product.builder().name("pID").description("dIT").build();
	}

}
