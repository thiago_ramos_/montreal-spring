package br.com.projeto.montreal;

import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.linkWithRel;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.links;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.LinkDiscoverers;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.hypermedia.LinksSnippet;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import lombok.Getter;

/**
 * The Class BaseWebTest.
 *
 * @author thiago
 * @since 13/05/2017
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public abstract class BaseWebTest {

	@Rule
	public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation(
			"target/generated-snippets");

	@Autowired
	LinkDiscoverers links;

	@Autowired
	LinkMatcher linkMatcher;
	



	


	
	protected final LinksSnippet pagingLinks = links(
			linkWithRel("first").optional().description("Primeira pagina de resultados."),
			linkWithRel("last").optional().description("Ultima pagina de resultados."),
			linkWithRel("next").optional().description("Proxima pagina de resultados."),
			linkWithRel("prev").optional().description("Pagina anterior de resultados."));

}
