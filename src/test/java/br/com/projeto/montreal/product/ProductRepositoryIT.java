package br.com.projeto.montreal.product;

import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.projeto.montreal.BaseMontrealTests;
import br.com.projeto.montreal.TestUtils;
import br.com.projeto.montreal.data.repository.IProductRepository;
import br.com.projeto.montreal.model.entities.Product;

/**
 * Integration test for {@link IProductRepository}.
 *
 * @author thiago
 * @since 13/05/2017
 */
public class ProductRepositoryIT extends BaseMontrealTests {

	@Autowired
	IProductRepository productRepository;

	@Test
	public void findAll_Success() {
		Iterable<Product> products = productRepository.findAll();

		assertThat(products, is(not(emptyIterable())));
	}

	@Test
	public void saveProduct_Success() {
		Product product = productRepository.save(TestUtils.buildProduct());
		Product newProduct = productRepository.findOne(product.getId());

		assertThat(product, is(Matchers.<Product>equalTo(newProduct)));
	}

	@Test
	public void updateProduct_Success() {
		Product product = productRepository.save(TestUtils.buildProduct());
		Product newProduct = productRepository.findOne(product.getId());

		assertThat(product, is(Matchers.<Product>equalTo(newProduct)));

		newProduct.setName("Update Teste");
		productRepository.save(newProduct);

		Product updatedProdut = productRepository.findOne(newProduct.getId());
		assertThat(newProduct.getName(),
				is(Matchers.<String>comparesEqualTo(updatedProdut.getName())));

	}

	@Test
	public void deleteProduct_Success() {

		Iterable<Product> products = productRepository.findAll();

		assertThat(products, is(not(emptyIterable())));

		for (Product product : products) {
			product.setSubProducts(null);
		}

		productRepository.delete(products);

		products = productRepository.findAll();

		assertThat(products, is(emptyIterable()));
	}
}
