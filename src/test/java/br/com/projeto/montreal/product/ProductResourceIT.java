package br.com.projeto.montreal.product;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.halLinks;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.linkWithRel;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.links;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.restdocs.snippet.Attributes.key;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.MediaType;
import org.springframework.restdocs.constraints.ConstraintDescriptions;
import org.springframework.restdocs.hypermedia.LinkDescriptor;
import org.springframework.restdocs.mockmvc.MockMvcRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.restdocs.payload.FieldDescriptor;
import org.springframework.restdocs.payload.JsonFieldType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.projeto.montreal.BaseWebTest;
import br.com.projeto.montreal.model.entities.Image;
import br.com.projeto.montreal.model.entities.Product;
import br.com.projeto.montreal.model.enums.MimeType;

/**
 * The Class ProductResourceIT.
 *
 * @author thiago
 * @since 16/05/2017
 */
public class ProductResourceIT extends BaseWebTest {

	@Autowired
	WebApplicationContext context;

	RestDocumentationResultHandler documentHandler;

	@Autowired
	private ObjectMapper objectMapper;

	protected MockMvc mvc;

	/**
	 * Sets the up.
	 */
	@Before
	public void setUp() {
		documentHandler = MockMvcRestDocumentation.document("{method-name}",
				preprocessRequest(prettyPrint()),
				preprocessResponse(prettyPrint()));
		mvc = MockMvcBuilders.webAppContextSetup(context)
				.apply(documentationConfiguration(restDocumentation))
				.alwaysDo(documentHandler).build();
	}

	/**
	 * Root resource products.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void rootResourceProducts() throws Exception {

		mvc.perform(RestDocumentationRequestBuilders.get("/"))
				.andDo(MockMvcResultHandlers.print()).andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaTypes.HAL_JSON))
				.andExpect(jsonPath("$._links.products.href", notNullValue()))
				.andDo(documentHandler.document(links(halLinks(),
						linkWithRel("products")
								.description(
										"Link para o resource de produtos."),
						linkWithRel("profile").description(
								"Profiles para os resources da api."))));
	}

	/**
	 * Gets the product.
	 *
	 * @return the product
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void getProduct() throws Exception {

		mvc.perform(RestDocumentationRequestBuilders.get("/products/1"))
				.andDo(MockMvcResultHandlers.print()).andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaTypes.HAL_JSON))
				.andDo(documentHandler
						.document(responseFields(getProductFieldDescriptors())))
				.andDo(documentHandler
						.document(links(halLinks(), getLinksDescriptor())));
	}

	/**
	 * List all.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void listAll() throws Exception {

		mvc.perform(RestDocumentationRequestBuilders.get("/products"))
				.andDo(MockMvcResultHandlers.print()).andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaTypes.HAL_JSON));
	}

	/**
	 * Creates the product.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void createProduct() throws Exception {

		ConstrainedFields fields = new ConstrainedFields(Product.class);

		Product product = new Product("Info", "Computer.",
				new Product("Pasta", "Jaiminho Collector´s Edition."),
				Arrays.asList(new Image(MimeType.TIFF)));

		mvc.perform(post("/products").contentType(MediaTypes.HAL_JSON_VALUE)
				.content(objectMapper.writeValueAsString(product)))
				.andExpect(status().isCreated())
				.andDo(documentHandler.document(requestFields(
						fields.withPath("name")
								.description("O nome do produto."),
						fields.withPath("description")
								.description("A descricao do produto."),
						fields.withPath("images")
								.description("As imagens do produto."),
						fields.withPath("parentProduct")
								.description("Produto pai."))));
	}

	/**
	 * Update product.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void updateProduct() throws Exception {

		ConstrainedFields fields = new ConstrainedFields(Product.class);

		Product product = Product.builder().name("pUpdate")
				.description("dUpdate").build();

		String productLocation = mvc
				.perform(
						post("/products").contentType(MediaTypes.HAL_JSON_VALUE)
								.content(objectMapper
										.writeValueAsString(product)))
				.andExpect(status().isCreated()).andReturn().getResponse()
				.getHeader("location");

		Map<String, Object> productUpdate = new HashMap<String, Object>();
		productUpdate.put("name", "novoNome");
		productUpdate.put("description", "novoNome");
		productUpdate.put("images",
				Collections.singletonList(new Image(MimeType.BMP)));

		mvc.perform(patch(productLocation).contentType(MediaTypes.HAL_JSON)
				.content(this.objectMapper.writeValueAsString(productUpdate)))
				.andExpect(status().isNoContent())
				.andDo(documentHandler.document(requestFields(
						fields.withPath("name").description("Nome do produto")
								.type(JsonFieldType.STRING).optional(),
						fields.withPath("description")
								.description("Descricao do produto")
								.type(JsonFieldType.STRING).optional(),
						fields.withPath("images")
								.description("Imagens do produto")
								.type(JsonFieldType.ARRAY).optional())));

	}

	/**
	 * Product custom api.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void productCustomApi() throws Exception {
		mvc.perform(RestDocumentationRequestBuilders.get("/products/search"))
				.andDo(MockMvcResultHandlers.print())
				.andDo(MockMvcResultHandlers.print()).andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaTypes.HAL_JSON))
				.andDo(documentHandler.document(
						links(halLinks(), getCustomSearchLinksDescriptor())));
	}

	/**
	 * Gets the product images.
	 *
	 * @return the product images
	 * @throws Exception
	 *             the exception
	 */
	@Test
	public void getProductImages() throws Exception {
		mvc.perform(RestDocumentationRequestBuilders.get("/products/6/images"))
				.andDo(MockMvcResultHandlers.print())
				.andDo(MockMvcResultHandlers.print()).andExpect(status().isOk())
				.andExpect(content()
						.contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
	}

	private FieldDescriptor[] getProductFieldDescriptors() {
		return new FieldDescriptor[] {
				fieldWithPath("name").description("Nome do produto"),
				fieldWithPath("description")
						.description("Descricao do produto"),
				fieldWithPath("images").description("Imagens do produto"),
				fieldWithPath("_links.self").ignored()
						.description("Link para o proprio produto"),
				fieldWithPath("_links.product").ignored()
						.description("Link para o proprio produto"),
				fieldWithPath("_links.parentProduct")
						.description("Link para o produto pai."),
				fieldWithPath("_links.subProducts")
						.description("Link para os sub produtos."),
				fieldWithPath("_links.images")
						.description("Link para as images do produto.")

		};
	}

	public LinkDescriptor[] getLinksDescriptor() {
		return new LinkDescriptor[] {
				linkWithRel("subProducts")
						.description("Link para os sub produtos."),
				linkWithRel("product").description("Link para o produto."),
				linkWithRel("self").description("Link para o produto.")
						.ignored(),
				linkWithRel("images")
						.description("Link para as images do produto."),
				linkWithRel("parentProduct")
						.description("Link para o pai do produto."), };
	}

	public LinkDescriptor[] getCustomSearchLinksDescriptor() {
		return new LinkDescriptor[] {
				linkWithRel("byParentId").description("Busca o pai pelo id."),
				linkWithRel("allFetchImages")
						.description("Busca todos os produtos e suas imagens."),
				linkWithRel("byIdFetchImages").description(
						"Busca as images de um determinado produto."),
				linkWithRel("byIdFetchSubProducts").description(
						"Busca as sub produtos de um determinado produto."),
				linkWithRel("allFetchSubProducts").description(
						"Busca todos os produtos e seus sub produtos."),
				linkWithRel("self").description("Link para o produto.")
						.ignored() };
	}

	private static class ConstrainedFields {

		private final ConstraintDescriptions constraintDescriptions;

		ConstrainedFields(Class<?> input) {
			this.constraintDescriptions = new ConstraintDescriptions(input);
		}

		private FieldDescriptor withPath(String path) {
			return fieldWithPath(path)
					.attributes(key("constraints")
							.value(StringUtils.collectionToDelimitedString(
									this.constraintDescriptions
											.descriptionsForProperty(path),
									". ")));
		}
	}

}
